var mqtt = require('mqtt');

const MQTT_SERVER = "127.0.0.1";
const MQTT_PORT = "1883";
//if your server don't have username and password let blank.
const MQTT_USER = "osboxes"; 
const MQTT_PASSWORD = "osboxes.org";

// Connect MQTT
var client = mqtt.connect({
    host: MQTT_SERVER,
    port: MQTT_PORT,
    username: MQTT_USER,
    password: MQTT_PASSWORD
});

client.on('connect', function () {
    // Subscribe any topic
    console.log("MQTT Connect");
    client.subscribe('sensors', function (err) {
        if (err) {
            console.log(err);
        }
    });
});

// Receive Message and print on terminal
client.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString());
});

